Warning
=======
This code comes without any claim on functionality

Purpose
=======
Take your ics calendar and make a nice yearly overview with events of 24 hours or more indicated

Installation
============

| Execute the following commands in a terminal
| sudo apt-get install -y ipython3 python3-matplotlib python-icalendar python3-icalendar
|    git clone git@gitlab.com:uranus/yearcalendar.git
|    cd yearcalendar
|    pip3 install .
(to follow any changes in the source-path: pip3 install -e .  )
(to upgrade even if the version number remained the same: pip3 install --upgrade .)

Hints
------
This installs also a shell command "ucalendar" to .local/bin. as this is not not by default in the $PATH in ubuntu, add this path to your ~/.bashrc file
        export PATH="$HOME/.local/bin:$PATH"

Uninstallation
==============
pip3 uninstall ucalendar

Usage
=====
* save your calendar to a ics file (e.g. in thunderbird: "events and tasks" -> "export"
* add a config file (see section config file), - example see: config directory
* optional: if requested in the config file, add files for sidelines - example see: config directory
* execute: ucalendar configfilename
    * optional: ucalendar configfilename year
* be patient, the parsing of the ics file may take long, really long,...
* The program calculates the number of holidays taken (excluding weekends and public holidays). For this to work, the holidays must be marked as cathegory  "vaccation" and publich holidays as cathegory "public holiday"


Known issues
============

* Depending on the size, parsing of the ics file takes quite long... 
* Special characters (e.g. special German letters) may cause issues

Config File
===========

Section general
| saveformat - define the fileformat e.g. pdf , svg... any matplotlib compatible format

Section eventcoloring
---------------------
The section eventcoloring contains a number of lines with regular expressions and corresponding colors. This is used to determine the background color of the event. The first line is the default colour.
    E.g. the line "genf : green" causes all entries containing the word "genf' to have a green background

Section hatchcoloring
---------------------
The background of any event can have hatching
    E.g: the line ".*with m.* = blue" causes all entries containing "with m." to have blue hatching

Section sidelines
-----------------
| You can have a coloured bar on the left side of each date indicate additional info e.g. blocked days etc. These dates are listed in a file with one date per line.
| Example input:
|    file1 = blocked.txt
|    color1 = red
|    width1 = 0.1
|    means that all dates indicated in the file blocked.txt will have a 0.1-wide, red bar on the left side

Section sidebars
-----------------
| You can have a coloured bar on the left side of the date for events that have a category information attached.
| Example config:
|    category1 = issues
|    color1 = red
|    width1 = 0.1
|    means that all events that are of category 'issues' 0.1-wide, red bar on the left side. Multiple categories can be inserted by category2, category3,...

Section filters
---------------
Allows to filter only selected entries by regular expression
Example config
type1 = CATEGORIES
value1 = Public Holiday


