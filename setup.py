from setuptools import setup

setup(name = 'ucalendar',
      version = '0.3.2',
      description = 'Build a yearly calendar from a ics file',
      url = 'https://gitlab.com/uranus/ucalendar',
      author = 'Ulrich Dorda',
      author_email = 'ulrich@dorda.net',
      license = 'GPLv2',
      packages = ['ucalendar'],
      install_requires = ['icalendar'],
      include_package_data = True,
      scripts=['scripts/ucalendar'],
      zip_safe = False)
