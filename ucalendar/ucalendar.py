# Python 3

# the matlotlib origin (0,0) is at the bottom left corner
# the upper end is thus at y=31
 
#import ics
import icalendar
import calendar
import pylab
import datetime
import time
import matplotlib
import numpy
import re
import sys
#import shutil
import os
import configparser


def addsidelinesfromfile(filename, year, color, width):
    """
    parses of file containing a list of dates (each date one line, e.g: 22.01.2017) and adds at each date a bar on the left side
    
    ! input:
        * filename
        * year
        * color
        * width - the width of the bar
    """

    if not os.path.isfile(filename):
        raise Exception('Error: File for sidelines not found: %s', filename)
    a = open(filename)

    for i in a.readlines():
        if i.strip() !='':
            match = re.match('(\d*).(\d*).(\d*)', i.strip())
            if not match:
                print('Error in line %s of sideline info'%i)
            curday = int(match.group(1))
            curmonth = int(match.group(2))
            curyear = int(match.group(3))
            if curyear == year:
                newrect = matplotlib.patches.Rectangle(
                    (curmonth - 1, 31 - curday),   # (x,y)
                    float(width),                         # width
                    1 ,                            # height
                    fill = color,
                    facecolor = color)
                pylab.gca().add_patch(newrect)    

# XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

def readcalendar(calname):
    """
    reads a .ics file and returns the data
    
    ! input:
        * calendar path
    """
    if not os.path.isfile(calname):
        raise Exception('Error: Calendar file does not exist: %s'%calname)
    
    o = open(calname)
    try:
        c = icalendar.Calendar.from_ical(o.read())
        o.close()
        return c
    except Exception as e:
        print('Error: Failed to parse the calendar file \n Error:\n', str(e))


# XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

def showcalendar(c, year, eventcolordict, hatchcolordict, sidebardict, filterlist):
    
    holidaylist = []
    pulicholidays = []
    start = datetime.datetime(year, 1, 1)

    # XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
 
    def boxdate(curmonth, curday):
        pylab.plot([curmonth - 1, curmonth     ], [31 - curday + 1, 31 - curday + 1], 'k', lw = 1)  # line above date
        pylab.plot([curmonth - 1, curmonth     ], [31 - curday    , 31 - curday    ], 'k', lw = 1)  # line below date
        pylab.plot([curmonth - 1, curmonth - 1 ], [31 - curday + 1, 31 - curday    ], 'k', lw = 1)  # line left of date 
        pylab.plot([curmonth    , curmonth     ], [31 - curday + 1, 31 - curday    ], 'k', lw = 1)

    # XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
    
    def filldate(start, end, fill = True, hatchcolor = None, facecolor = 'g', alpha = 1, width = 1):
        
        ax1 = pylab.gca()
    
        curmonth = start.month
        curday = start.day
        #all day events do not have hours.... set to 0

        duration = end - start
        duration = duration.days + duration.seconds / 3600. / 24.
        

        if hatchcolor != None:
            hatch = '/'
        else:
            hatch = None
            
        #check if duration is within the months length
        if end.month > start.month:
            #going into the next month as well
            
            # plot the part in the first month
            daysincurrentmonth = calendar.monthrange(year, curmonth)[1]
            #daysinthismonth = daysincurrentmonth - curday
            #daysinnextmonth = duration -(calendar.monthrange(year, curmonth)[1] - curday) 
            ax1.add_patch(
                matplotlib.patches.Rectangle(
                (curmonth - 1, 32 - curday - start.hour/24.),   # (x,y)
                width,                  # width
                - (32 - curday - start.hour/24.) + (31 - daysincurrentmonth) ,          # height
                fill = fill,
                facecolor = facecolor,
                alpha = alpha,
                hatch = hatch,
                edgecolor = hatchcolor
                ))
            # plot the part in the next month
            ax1.add_patch(
                matplotlib.patches.Rectangle(
                (curmonth, 31),            # (x,y)
                width,                         # width
                #31 - curevent.end.day,
                - end.day + 1 - end.hour/24.,          # height
                fill = fill,
                facecolor = facecolor,
                alpha = alpha,
                hatch = hatch,
                edgecolor = hatchcolor
                ))
            duration = calendar.monthrange(year, curmonth)[1] - curday + 1
        else:
            # the whole event is within one month
            ax1.add_patch(
                matplotlib.patches.Rectangle(
                (curmonth - 1, 32 - curday - start.hour/24.),   # (x,y)
                width,                  # width
                - duration,          # height
                fill = fill,
                facecolor = facecolor,
                alpha = alpha,
                hatch = hatch,
                edgecolor = hatchcolor
                ))

    # XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
    
    def insertNewlines(text, lineLength):
        #return text
        if len(text) <= lineLength:
            return text
        else:
            return text[:lineLength] + '\n' + insertNewlines(text[lineLength:], lineLength)

    # XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
    # plot the calendar BACKGROUND
    # XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX        
    if calendar.isleap(start.year):
        nrdays = 366
    else:
        nrdays = 365

    #pylab.plot a grid for days and months
    pylab.figure(figsize = (24,12.725))
    for i in range(13):
        pylab.plot([i,i],[0,31], 'k', lw = 0.1)

    for i in range(32):
        pylab.plot([0,12],[i, i], 'k', lw = 0.1)

    for curmonth in range(1, 13):
        #pylab.plot a line indicating the end of the month
        pylab.plot([curmonth - 1, curmonth], [31 - calendar.monthrange(year, curmonth)[1], 31 - calendar.monthrange(year, curmonth)[1]], 'k', lw = 3)
        # indicate the weekends 
        for curday in range(1, calendar.monthrange(year, curmonth)[1] + 1):
            if calendar.weekday(year, curmonth, curday) == 5 or calendar.weekday(year, curmonth, curday) == 6:
                #boxdate(curmonth, curday)
                pylab.gca().add_patch( matplotlib.patches.Rectangle((curmonth - 1, 31 - curday), 1, 1, facecolor = 'gray', alpha = 0.2))
            # write the date and weekday into each field
            weekdaydict = {0 : 'Mo', 1 : 'Tu', 2 : 'We', 3 : 'Th', 4: 'Fr', 5 : 'Sa', 6 : 'Su'}
            weekday = weekdaydict[calendar.weekday(year, curmonth, curday)]
            weekday = ' %s %s'%(curday, weekday)
            pylab.text(curmonth - 1, 31 - curday + 0.2, weekday, fontsize = 20, color = 'gray', alpha = 0.5)            

    pylab.xlim(-0.5,12.5)
    pylab.ylim(-0.5, 31.5)
    month_labels = numpy.array(['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'])
    pylab.xticks(numpy.arange(12) + 0.5, month_labels, fontsize = 20)
    pylab.yticks(numpy.arange(31) + 0.5, 31 - numpy.arange(31), fontsize = 20)

    # XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

    def eventcolor(c_event):
        """returns the color in which to color the event"""
        for curkey in list(eventcolordict.keys()):
            if re.match(curkey, c_event['SUMMARY'], re.IGNORECASE) or ('LOCATION' in c_event and re.match(curkey, c_event['LOCATION'], re.IGNORECASE)):
                outcolor = eventcolordict[curkey]
        return outcolor

    # XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

    def hatchcolor(event):
        """returns the color in which to color hatching of the event"""
        outcolor = None
        for curkey in list(hatchcolordict.keys()):
            if re.match(curkey, event['SUMMARY'], re.IGNORECASE):
                outcolor = hatchcolordict[curkey]
        return outcolor

    # XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

    #filter for events
    for i, c_event in enumerate(c.subcomponents):
        if 'DTSTART' in c_event and 'DTEND' in c_event and 'SUMMARY' in c_event:
            pass
        else:
            continue
        
        # see if there are filters, if yes, check if the event passes it
        if len(filterlist) > 0:
            tmp = False
            for cfilter in filterlist:
                if cfilter[0] in c_event:
                    if type(c_event[cfilter[0]]).__name__ == 'list':
                        tmplist = c_event[cfilter[0]] 
                    else:
                        tmplist = [c_event[cfilter[0]]]
                    for i in tmplist:
                        if re.match(cfilter[1], i):
                            tmp = True
            if not tmp:
                continue

        # make sure the cathegories attribute is always a list (if it exists at all)
        if 'CATEGORIES' in c_event:
            #entries of registered categories get sidebars even if they are shorter than 1 day
            if type(c_event['CATEGORIES']).__name__ != 'list':
                c_event['CATEGORIES'] = [str(c_event['CATEGORIES'].cats[0])]
            else:
                c_event['CATEGORIES'] = [str(tmp.cats[0]) for tmp in c_event['CATEGORIES']]
        else:
            c_event['CATEGORIES'] = []
        start = c_event['DTSTART'].dt
        if hasattr(start, 'tzinfo'):
            start = start.replace(tzinfo=None)
        end = c_event['DTEND'].dt
        if hasattr(end, 'tzinfo'):
            end = end.replace(tzinfo=None)
        if type(start).__name__ != 'datetime':
            start = datetime.datetime.combine(start, datetime.datetime.min.time())
        if type(end).__name__ != 'datetime':
            end = datetime.datetime.combine(end, datetime.datetime.min.time())

        name = c_event['SUMMARY']
        duration = end - start

        if duration.days >= 1:
            
            # XXXXXXXXXXXXXXXXXXXXXXXX
            # The MAIN COLORING

            if start.year == year or end.year == year:
                # if the end date is beyond this year, reset it to the end of the year
                if end.year > year:
                    end = datetime.datetime(year, 12, 31, 23, 59, 59)
                if start.year < year:
                    start = datetime.datetime(year, 1, 1, 0, 0, 0) 
                   

                duration = end - start  #recompute to have correct data in case start or end was changed

                # If there are question marks in the name, make it morer opaque
                if re.match('.*\?\?.*', name):
                    alpha = 0.3
                else:
                    alpha = 0.6
                
                filldate(start, end, fill = True, facecolor = eventcolor(c_event), hatchcolor = hatchcolor(c_event), alpha = alpha)
                
                textstart = 32 - start.day - start.hour / 24.
                #remove all letters and symbols but basic letters (e.g. no german umlaute...)
                eventname = re.sub('[^a-z,^?,A-Z,0-9,\s]','',name)
                eventname = re.sub('$','',eventname)
                if 'LOCATION' in c_event:    
                    eventname = eventname + '(%s)'%c_event['LOCATION']
                pylab.text(start.month - 1, textstart, insertNewlines(eventname, 14), fontsize = 16, verticalalignment = 'top')

        # XXXXXXXXXXXXXXXXXXXXXXXX
        # SIDEBARS
        if start.year == year or end.year == year:
            if 'CATEGORIES' in c_event:
 
                #entries of registered categories get sidebars even if they are shorter than 1 day
                for c_category in sidebardict:
                    for c_comp_category in c_event['CATEGORIES']:
                        if type(c_comp_category).__name__ == 'str' and c_comp_category.lower() == c_category:
                            filldate(start, end, fill = True, hatchcolor = None, facecolor = sidebardict[c_category]['color'], alpha = 1, width = float(sidebardict[c_category]['width']))
                            
                if c_event['CATEGORIES'] == 'Vacation':
                    for i in range(duration.days + 1):
                        c_date = start + datetime.timedelta(days = i)
                        # start after 16:00 -> no vaccation day
                        if c_date == start and start.hour > 16:
                            continue
                        # back before 11:00 -> no vaccation day
                        if end.hour < 11:    
                            continue
                        # ignore weekends
                        if calendar.weekday(c_date.year, c_date.month, c_date.day) == 5 or calendar.weekday(c_date.year, c_date.month, c_date.day) == 6:
                            continue
                        if c_date.year != year:
                            continue
                        #print('Vaccation found :',c_date)
                        if type(c_date).__name__ == 'datetime':
                            c_date = c_date.date()
                        holidaylist.append(c_date)

                #if c_event['CATEGORIES'] == 'Public Holiday':
                #    for i in range(duration.days + 1):
                #        c_date = start + datetime.timedelta(days = i)
                #        if c_date == end and endhour == 0:
                #            continue
                #        pulicholidays.append(c_date)
    

    #holidaylist.sort()
    pulicholidays.sort()
    #remove all public holidays from the holiday list
    #for c_date in pulicholidays:
    #    #simple comparisson is not sufficent, as objects differ if e.g. tzinfo is given or not, hour is given or not...
    #    for c_holiday in holidaylist:
    #        if c_date.month == c_holiday.month and c_date.day == c_holiday.day:
    #            holidaylist.remove(c_holiday)
    
    #print('public holidays: ')
    #for c_date in pulicholidays:
    #    print(c_date)
    
    print('Nr of holidays', len(holidaylist))
    for c_date in holidaylist:
        print(c_date)

    pylab.gca().xaxis.tick_top()
    pylab.tight_layout()
    pylab.ion()    
    pylab.show()
    pylab.draw()

    #return holidaylist, pulicholidays

# XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

def run(configfilename, year = False):
    """the main function, runs everything
    
    ! input:
        * the filename of the config file
    
    !optional input:
        * year = 2018 - this overwrites the year in the config file
    """
    
    #parse the config file
    config = configparser.ConfigParser()
    config.read(configfilename)
    
    if year:
        year = int(year)
    else:
        year = int(config.get('global', 'year'))
    #use os.path.expanduser to allow using the ~ to indicae the homedirectory
    calendarfilepath = os.path.expanduser(config.get('global', 'icsfilepath'))

    print('starting parsing ics file, this may take some time...')
    c = readcalendar(calendarfilepath)
    print('Done parsing ICS file')


    eventcolordict = config._sections['eventcoloring']
    hatchcolordict = config._sections['hatchcoloring']
    #publicholidaycathegory = config.get('global', 'publicholidaycathegory')

    #prepare the dicts for the sidebars
    if 'sidebars' in config._sections:
        sidebardict = {}
        for i in range(0, 9):
            if 'category%d'%i in config._sections['sidebars']:
                tmp = {'color' : config._sections['sidebars']['color%d'%i],
                       'width' : config._sections['sidebars']['width%d'%i]                       
                       }
                sidebardict[config._sections['sidebars']['category%d'%i].lower().strip()] =  tmp
    else:
        sidebardict = {}  
    
    if 'filters' in config._sections:
        filterlist = []
        for i in range(0, 9):
            if 'type%d'%i in config._sections['filters']:
                filterlist.append([config._sections['filters']['type%d'%i], config._sections['filters']['value%d'%i]])
    else:
        filterlist = {}  
    
    
    showcalendar(c, year, eventcolordict, hatchcolordict, sidebardict, filterlist)

    if 'sidelines' in config._sections:
        sidelinedict = config._sections['sidelines']
        for i in range(0, 9):
            filehook = 'file' + str(i)
            if filehook in sidelinedict:
                filename = config.get('sidelines', filehook)
                print(filename)
                color = config.get('sidelines', 'color' + str(i))
                width = config.get('sidelines', 'width' + str(i))
                #gca = pylab.gca()
                addsidelinesfromfile(filename, year, color, width)
                
    print('Done showing calendar')
    time.sleep(1)

    if 'savetopath' in config['global']:
        if 'saveformat' in config['global']:
            saveextension = config.get('global', 'saveformat')
        else:
            saveextension = 'pdf'
            
        savepath = os.path.expanduser(config.get('global', 'savetopath'))
        savename = os.path.join(savepath, '%s-yearplan.%s'%(str(year), saveextension))
        pylab.savefig(savename)
        print('Done saving calendar')
        if 'triggercommand' in config['global']:
            triggercommand =  config.get('global', 'triggercommand')
            triggercommand = triggercommand.replace('savetopath', savename)
            print('exec command', triggercommand)
            os.system(triggercommand)
            print('Done triggering command')


# XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

if __name__=='__main__':
    if len(sys.argv) == 3:
        run(sys.argv[1], year = sys.argv[2])
    else:
        run(sys.argv[1])

